var should = require('should');
var fs =  require('fs');

var analyzer = require('../index');

var textSample =  fs.readFileSync('test/text.txt',{ encoding:'UTF8' });

describe("Text stats",function(){

	it('Should report the correct number of sentences (19)', function(done){
		analyzer.stats(textSample).sentences.should.equal(19);
		done();
	});

	it('Should report the correct number of words (230)', function(done){
		analyzer.stats(textSample).words.should.equal(230);
		done();
	});

	it('Should report the correct number of syllables (326)', function(done){
		analyzer.stats(textSample).syllables.should.equal(326);
		done();
	});

	it('Should report the correct number of characters (952)', function(done){
		analyzer.stats(textSample).characters.should.equal(952);
		done();
	});

	it('Should report the correct carpar index (4.1)', function(done){
		analyzer.stats(textSample).carpar.should.equal('4.1');
		done();
	});

	it('Should report the correct Gulpease index (72)', function(done){
		analyzer.stats(textSample).gulpease.should.equal(72);
		done();
	});

	it('Should report the correct Gunnings Fog index (4)', function(done){
		analyzer.stats(textSample).gunningFog.should.equal(4);
		done();
	});

});