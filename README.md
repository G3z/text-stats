## Readability stats on text

Simple utility to get stats on a text.
available as a cli or as module

# Installation

    npm install --save text-stats
    npm install -g text-stats


# Usage API

    var text = "this is a text.\nAnd also a test";
    var stats = textStats.stats(text);
    var wordCount = stats.words;

# Usage CLI

    $ text-stats test/text.txt
	- sentences: 19
	- words: 230
	- syllables: 326
	- characters: 952
	- carpar: 4.1
	- gulp: 72
	- gunn: 4

# Properties

sentences: Number of sentences  
words: Number of words  
syllables: Number of syllables  
characters: Number of characters  
carpar: Character per paragraph  
gulpease: [Gulpease Index](http://wikipedia.qwika.com/it2en/Indice_Gulpease)  
gunningFog: [Gunning fog index](http://en.wikipedia.org/wiki/Gunning_fog_index)